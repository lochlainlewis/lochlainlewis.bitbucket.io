"""
Author: Lochlain Lewis  Date: 08/14/2018

source: https://www.seas.upenn.edu/~matuszek/cit591-2008/
Assignments/02-lunar-lander.html

This is a Python version of the assignment found on Upenn CIS591
assignments page. This version is modified from the version indicated in
the instructions. Some of the variables are different but do the same thing.
At 1.5 burn constant and 1.6 for the gravity factor the game is fairly easy, if
not slightly boring.

This game is a loose representation of one of the earliest computer games.
The assignment calls for a Java program. I used Python in this version because
I'm not that familiar with Python.
"""
import time
import sys

burnConstant = 1.5
burnAmount = 0
startingAltitude = 1000
gravityKFactor = 1.6
startingFuel = 1000
burnBoolean = False

def main():
    
    PrintGreeting()
    
def PlayerAnswer(playerResponse):
    
    answerKey = False
    
    if(playerResponse == "yes" or playerResponse == "Yes"):
        answerKey = True
        return answerKey
    else:
        return answerKey
    
def StartMessage():
    
    print("We start 1000 meters above the surface. Our velocity is 0 m/sec.")

    time.sleep(1)

    print("\nWe have 1000 pounds of fuel onboard.\n")

    print("You're the mission commander...\n")

    time.sleep(1)

    print("You need to decide how much fuel you want to burn.\n" +
           "Our constant for this mission is 1.5. So to calculate\n" +
           "the affect the burn will have on descent multiply\n" +
           "the constant by the burn amount.")

    GameLoop()
    
def EndMessage():
    print("\n\nOkay, closing game now...\n" +
          "Have a nice day!")
    sys.exit(0)

def PrintGreeting():
    playerName = input("Hi! What's your name? ")
    print("Nice to meet you " + playerName + "!")
    playGameAnswer = input("Would you like to play Lunar Lander? ")
    if(PlayerAnswer(playGameAnswer) == True):
        StartMessage()
    elif(PlayerAnswer(playGameAnswer) == False):
        EndMessage()

def GetBurnAmount():
    value = 0
    while True:
        try:
            value = input("Enter amount of fuel to burn: ")
            value = int(value)
            break
        except ValueError:
            print("Not a valid integer. Please try again.")
        print("You entered an integer.")
    return value

def GetThrustFactor(newBurnAmount):
    thrust =  int(newBurnAmount) * burnConstant
    return thrust

def GetVelocity(timeHack):
    newVelocity = gravityKFactor * timeHack
    return newVelocity
    
def GameLoop():
    
    currentAltitude = startingAltitude
    availableFuel = startingFuel
    accelerationFactor = 0
    oldVelocity = 0
    residualVelocity = 0
    adjustedVelocity = 0
    
    while(currentAltitude > 0):
        
        accelerationFactor += 1
        newBurn = GetBurnAmount()
        availableFuel = availableFuel - int(newBurn)
        thrust = GetThrustFactor(newBurn)
        newVelocity = GetVelocity(accelerationFactor)
        print("thrust: " + str(thrust) + "\n" + "newVelocity: " + str(newVelocity)
              + "\n" + "residualVelocity: " + str(residualVelocity)
              + "\n" + "accelerationFactor: " + str(accelerationFactor))
        
        if(residualVelocity > 0 and thrust == 0):
            adjustedVelocity = residualVelocity + newVelocity
            residualVelocity = adjustedVelocity
            #print("\nadjustedVelocity: " + str(adjustedVelocity))
        else:
            adjustedVelocity = newVelocity - thrust       

        if(adjustedVelocity <= 0 or thrust > 0):
            accelerationFactor = 0
            if(residualVelocity > 0):
                adjustedVelocity = residualVelocity - thrust
            else:
                adjustedVelocity = newVelocity - thrust
            residualVelocity = adjustedVelocity

        print("\naccelationFactor: " + str(accelerationFactor)
              + "  " + "newBurn: " + str(newBurn)
              + "  " + "thrust: " + str(thrust)
              + "  " + "newVelocity: " + str(newVelocity)
              + "  " + "residualVelocity: " + str(residualVelocity))

        currentAltitude = currentAltitude - adjustedVelocity

        print("\nAltitude: " + str(currentAltitude) + "  " + "Velocity: "
              + str(adjustedVelocity) + "  " + "Fuel: " + str(availableFuel)
              + "  " + "Burn: " + str(newBurn))

        if(currentAltitude <= 0):
            print(".\n.\n.\n.\n.\n.\n.\n.\n")
            if(adjustedVelocity < 11):
                print("Congratulations! You landed safely!")
                print("\nAltitude: 0" + "  " + "Velocity: 0"
                    + "  " + "Fuel remaining: "
                    + str(availableFuel))
            elif(adjustedVelocity > 10 and adjustedVelocity < 15):
                print("Hope you packed a lunch. The LM is damaged but repairable.")
                print("You made a new crater about 5 meters across.")
            elif(adjustedVelocity > 15 and adjustedVelocity < 30):
                print("Ouch! Get some Band-aids. The crew is injured." +
                      "\nThe LM is beyond repair.")
                print("You made a new crater about 15 meters across.")
            else:
                print("Death!")
                print("You made a new crater about 35 meters across.")
            break

main()



