Just a quick game app in Python to work on some Python basics and have fun
with one of the first computer games. This version is just a console output version,
no GUI. 

### How do I get set up? ###

To run this game you need to have some form of Python environment. Clone or copy the application
into your IDE and run it like any other Python program. This application is built and runs on 3.7.2.
It might work on earlier verisons as well but it hasn't been tested. Since it doesn't use any complex
libraries or have dependencies it should be fine.

The source website is in the application documentation at the top of the page.

### Contribution guidelines ###

None.

### Who do I talk to? ###

Me