class ScoreClass:

    def __init__(score, humanScore, computerScore):
        score.humanScore = humanScore
        score.computerScore = computerScore
        score.updatedHumanScore = 0
        score.updatedComputerScore = 0

    def CalculateHumanScore(score, newScore):
        score.humanScore += int(newScore)
        updatedHumanScore = score.humanScore
        print("Updated human score: ", updatedHumanScore)

    def CalculateComputerScore(score, newScore):
        score.computerScore += int(newScore)
        updatedComputerScore = score.computerScore
        print("Updated computer score: ", updatedComputerScore)

    def __repr__ (score):

        if(score.humanScore > score.computerScore):
            print("You won!")
        else:
            print("You lost!")
        return ("Human score: {}; Computer score: {}"
                    .format(score.humanScore, score.computerScore))
