import random

class DieControl():

    def RollAgain(self):
        playerAnswer = input("Do you want to roll again? ")

        if(playerAnswer == "Yes" or playerAnswer == "yes"
           or playerAnswer == "y" or playerAnswer == "Y"):
            return True

        return False

    def Roll(self):
    
        return random.randrange(1, 7, 1)
