class PrintGameInstructions:

    def gameInstructions(self):
    
        instructions = """\n\tThe Rules for playing Pig:
        You're playing against the computer.
        A turn is defined as when you have control of the die.
        while it's your turn you may roll the die as many times
        as you like. The numbers you roll are added to your game
        total. If you roll a one before relinquishing control of
        the die, your total for the turn goes to zero and you give up
        control of the die.
        \n\tThe first one to reach 100 wins the game.
        However, the other player has one opportunity to make it to 
        100. If they do, then play continues for one more rotation
        until one player has decisively more points than the other.\n"""

        return instructions
