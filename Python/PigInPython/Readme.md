Pig is a game of chance that involves one die.

In this rendition of the game the user plays the computer.
Game play begins with each player having zero points. The
object of the game is to reach the max number of points first.
Normally the game is played to 100 points. In the interest
of time and in consideration of the debugging process the
maximum number of points was set at 20. 

When it's a player's turn the player chooses whether or not
to roll the die. If the player passes, control of the die is 
passed to the other player. If the player rolls the die, the 
number showing on the die is added to the player's score for 
that turn. The player may choose to roll as many times as 
they want. With each successive roll the number showing on 
the die is added to the player's score. 

If during their turn a player rolls a 1, the player loses 
control of the die and loses all of the points for the turn.

If the player relinquinshes control of the die, all of the 
points accumulated for that turn are added to the players
total number of points. 