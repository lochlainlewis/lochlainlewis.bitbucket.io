def IsGameOver(humanScore, computerScore):

    gameOver = False

    if(humanScore > 20 or computerScore > 20):
        if(humanScore > 20):
            gameOver = True
            print("You won!")
            return gameOver
        elif(computerScore > 20):
            gameOver = True
            print("You lost!")
            return gameOver
        
    elif(humanScore > 20 and computerScore > 20):
        if(humanScore > computerScore):
            gameOver = True
            print("You won!")
            return gameOver
        elif(humanScore < computerScore):
            gameOver = True
            print("You lost!")
            return gameOver
    
    return gameOver
