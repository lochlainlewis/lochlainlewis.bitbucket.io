# Author: Lochlain Lewis
# Date: 2018.10.14 Version 0.2
#
# This game applicaton was derived from an assignment found at
# http://www.cis.upenn.edu/~matuszek/cit590-2016/Assignments/02-pig.html
# an assignment for a computer science class at Penn.
# I found this while searching for interesting CS problems to expand my
# knowledge of Python.
#
# Please see the first version for specific game instrustions, or read the
# PrintInstructions() function. This game is modified. The winning score is
# 20 instead of 100. To make the game go faster during the debugging process,
# 20 seemed like a realistic winning number; not too far and not so short that
# it would difficult to get either player to win.
#
# Naming convention: Classes and functions are camel-case beginning with an
# uppercase letter. In addition to the obvious type definitions, classes
# contain the word Class in the name to differentiate
# them from functions. Variables are camel-case, beginning with a lowercase
# letter.
#
# Changes in this version from the previous: function name changes in ScoreClass
# and in several of the fucntions made it easier to understand what they did
# just by reading the function name. ShowResults() function was eliminated since
# the functionality is handled in the HumanTurn() function.
#
# The HumanTurn() and ComputerTurn() functions contain mostly redundant functionality.
# Version 0.3 will address the practice of code reuse, collapse the redundancy into
# a single function that calculates the turn value for which ever function calls it;
# either the HumanTurn() or the ComputerTurn().

from Human import *

def Main():

    print(PrintGameInstructions().gameInstructions())
    HumanTurn()

Main()
