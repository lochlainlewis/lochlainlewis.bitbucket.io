Caesar Cipher is a simple cryptographic program based on the 
original cipher by the namesake Julius Caesar. This cipher is
perhaps the HelloWorld of the cryptology field and is the Python
representation of a program I built in Java in 2014. This version
also incorporates a random keycode generator and further encoding
using ASCII decimal conversions for each character in the 
cipher. The keycode is then hidden in the string of numbers.

Cipher encryption works by selecting any number and then shifting the
characters of a message to the right by that many letters. Thus,
if the key is 3 and there is an 'a' in the message then the 'a'
becomes a 'd'. The final step is to convert each letter to its ASCII
decimal equivalent, resulting in a string of digits.

In this application the term 'key' is referred to
as a keycode because the key is hidden in the final form of the 
encrypted message. This is done because the key is not hard-coded in
the application and therefore needs to be transmitted with the 
message in order for the recipient to decrypt the message. 

The result of encryption for any length string is a string of numbers. 
Although this application does not incorporate separate decryption,
the user could separate the encryption code from the decryption
code and feed an encrypted message into the decryption portion
of the code.