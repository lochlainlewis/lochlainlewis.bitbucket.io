import Encoder
import Decoder

def getUserInput():

    print("Enter a string, sentence or phrase to encoded: ")
    newString = input()
    return newString

newUserString = getUserInput()
encodedString = Encoder.getEncodedString(newUserString)
print('encoded string: ', encodedString)

Decoder.getDecodedString(encodedString)
