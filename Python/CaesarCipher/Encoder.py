import KeyFactory
import re
# import re is to access the regular expression library

def getEncodedString(newString):

    tempString = ''
    # get the new encoding key
    newKey = KeyFactory.getNewKey()
    #print(newKey)

    if(newKey > 26):
        newKey = newKey % 26
        #print('Encoder newKey: ', newKey)

    # delete non-alphanumerics, spaces, and set to uppercase
    newString = re.sub('[^a-zA-Z 0-9 , .]', '', newString).replace(' ', '').upper()

    for character in newString:

        # add encoded key to decimal equivalent of character
        # ord() function converts character to unicode (ASCII) equivalent
        newChar = ord(character) + newKey

        # loop around to beginning of uppercase of ASCII table
        if(newChar > 90):
            newChar = (newChar - 90) + 32 #64

        # convert newChar int to a string
        # and concatenate it to the final string
        tempChar = str(newChar)
        tempString += tempChar

    tempString += str(newKey)
    #print(tempString)

    return tempString


someString = "Some string to encode!"
getEncodedString(someString)
