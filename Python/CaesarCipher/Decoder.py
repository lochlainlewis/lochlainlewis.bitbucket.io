def getDecodedString(newString):

    finalString = ''
    
    if(len(newString) % 2 == 0):
        
        encodeKey = int(newString[-2 : ])
        newString = newString[:-2]
        #print('Decode newString ', newString, '\tencode key ', encodeKey)
        
    else:
        encodeKey = int(newString[-1])
        newString = newString[:-1]
        #print('Decoder ', newString, '\tEncode key ', encodeKey)


    strLimit = len(newString) / 2
    #print(len(newString), '\t', strLimit)

    while(strLimit > 0):

        # gets the first character from the front of the string
        nextSubString = int(newString[:2])
        
        # loop around if less than lowest character on ASCII table
        if((nextSubString - encodeKey) < 33):
            decodeString = 91 - (encodeKey - (nextSubString - 33))
        
        else:
            decodeString = nextSubString - encodeKey

        # Chop off the first character from the input string,
        # we're finished with it.
        newString = newString[2:]

        # Convert the decimal to its ASCII character equivalent
        # and add it to the final string
        finalString = finalString + chr(decodeString)

        strLimit -= 1
    
    print('\n\nDecoded string: ', finalString)
