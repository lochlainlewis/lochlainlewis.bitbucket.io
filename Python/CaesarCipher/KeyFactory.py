import datetime


def getNewKey():

    # get current date/time as just a string of characters
    now = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    now = int(now)
    sumNumber = 0

    # sum the digits in now
    while(now > 0):
        interim = now % 10
        sumNumber += interim
        now /= 10

    sumNumber = int(sumNumber)
    return sumNumber

