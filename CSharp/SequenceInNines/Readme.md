The SequenceInNines application is a trivial program in C#.

Developed 02/14/2020. By Lochlain Lewis with the assistance of 
Jasmine Ragan. 

Inspiration for this program resulted from a discussion with
Jas about the perpetuation of 9s in summing the digits of 
multiples of 9. A true sum of digits is executed by summing
the digits of a number until one digit remains. Multiples
of 9 always result in a sum of digits = 9.

In order to see the pattern of the shift that occurs from single
to double digits, first-tier sums, we only execute the first sum
process:

Thus; 9 * 555 = 4995, 4 + 9 + 9 + 5 = 27 we stop the process
and record the 27 so that we can see when the shift to 36 occurs.

Moreover, we print all digits of the same value on one
line and shift lines when a new value emerges.

The result is an interesting pattern that when considering wave
theory we can imagine the pattern, sans the values of the integers
making the pattern, represents a primary wave and a half-wave-shifted,
destructive, harmonic wave of half-amplitude strength.

Sidebar: the proof for this pattern is given at the mathforum website:
http://mathforum.org/library/drmath/view/67061.html
