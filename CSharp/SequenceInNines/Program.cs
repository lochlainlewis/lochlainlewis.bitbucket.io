﻿using System;

namespace SequenceInNines
{
    class Program
    {
        static void Main(string[] args)
        {
            RunSequenceInNines newSequence = new RunSequenceInNines();

            Console.WriteLine("Enter max number: ");
            int seedNumber = Convert.ToInt32(Console.ReadLine());
            newSequence.CalculateNewSequence(seedNumber);
        }
    }
}
