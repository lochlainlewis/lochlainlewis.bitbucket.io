using System;

namespace SequenceInNines
{
    class RunSequenceInNines
    {
    	/* Multiply the newNumber by 9.
    	 * Pass the result to SumDigits. 
    	 */
    	internal int TimesNine(int newNumber)
    	{
    		RunSequenceInNines newAddition = new RunSequenceInNines();
    		newNumber *= 9;
    		newNumber = newAddition.SumDigits(newNumber);
    		return newNumber;
    	}

    	/* Get the sum of the digits in the number. 
    	 * return the result to the CalculateNewSequence
    	 * function for printing to the console. 
    	 * This function only iterates once through the
    	 * sum of digits process - which should result in
    	 * a single digit number. In the case of multiples
    	 * 9 the complete process will render 9. But in order
    	 * to reveal the pattern hidden in this process we only
    	 * want the first iteration result.
    	 */
    	internal int SumDigits(int newNumber)
    	{
  			int theSum = 0, modulusNumber = 0;
    		while(newNumber > 0)
    		{
    			modulusNumber = newNumber % 10;
    			theSum += modulusNumber;
    			newNumber /= 10;
    		}
    		return theSum;
    	}

    	/* Apply the number from the main method as the upper limit
    	 * to the for loop.
    	 * Pass the numbers up to 1 + the newNumber to the 
    	 * TimesNine function. 
    	 * Each time the result from the SumDigits function changes
    	 * a new line is added. Digits of equal value are placed on 
    	 * one line.
    	 */
        public void CalculateNewSequence(int newNumber)
        {
        	RunSequenceInNines newSequence = new RunSequenceInNines();
        	int shiftkey = 0;
            for(int i = 0; i < newNumber + 1; i++)
            {
            	if(newSequence.TimesNine(i) != shiftkey)
            	{
            		Console.WriteLine();
            	}
            	shiftkey = newSequence.TimesNine(i);
            	Console.Write(newSequence.TimesNine(i) + " ");
            }
        }
    }
}