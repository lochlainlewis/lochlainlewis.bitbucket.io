The SqlConnection application is a trivial program in C#.

Developed 02/12/2020. By Lochlain Lewis. 

This program is just a test program for developing a larger 
application that will collect data points from a sensor system
and upload them to a local MySQL database that currently resides
on a Raspberry PI. The process is currently handled by a Java
application, that while perfectly capable is a little dated and 
more combersome and restrictive than what is possible with a
C# program.
