using System;
using MySql.Data.MySqlClient;

namespace SqlConnection
{
	class NewConnection
	{
		public void SayAgain()
		{
			Console.WriteLine("Hello again!");
		}
		
		public void MakeConnection()
		{
			Console.WriteLine("Hello here.");
			string cs = @"server=localhost;userid=root;password=Cat5paw!;database=testData";
        	using var con = new MySqlConnection(cs);
        	con.Open();
        	Console.WriteLine($"MySqL version : {con.ServerVersion}");
        	con.Close();
		}
        
	}
}