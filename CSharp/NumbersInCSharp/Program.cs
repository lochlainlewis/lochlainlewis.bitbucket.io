﻿using System;
using System.Collections.Generic; // necessary to use List

namespace NumbersInCSharp
{

    public class NewClass
    {
        private int value = 10;
        public int GetValue()
        {
            return this.value;
        }
    }

    public class Program
    {
    	static void SomeMathStuff()
    	{
    		int seven = 7;
    		int four = 4;
    		int three = 3;
    		int quotientOne = (seven + four) / three;
    		int quotientTwo = (seven + four) % three;
    		Console.WriteLine($"quotient: {quotientOne},");
    		Console.WriteLine($"remainder: {quotientTwo}");
    	}

    	static void MinAndMaxValues()
    	{
    		int maxInt = int.MaxValue;
    		int minInt = int.MinValue;
    		double maxDouble = double.MaxValue;
    		double minDouble = double.MinValue;
    		decimal maxDecimal = decimal.MaxValue;
    		decimal minDecimal = decimal.MinValue;
    		Console.WriteLine($"The range of int is: {minInt} to {maxInt}.");
    		Console.WriteLine($"The range of double is: {minDouble} to {maxDouble}.");
    		Console.WriteLine($"The range of decimal is: {minDecimal} to {maxDecimal}.");
    	}

    	static void DoWhileLoop()
    	{
    		int count = 1;
    		do
    		{
    			Console.WriteLine("This prints once even if the condition is false!\n" +
    				"So it will print once more than number of times the condition calls for.");
 				count++;
    		} while(count < 1);
    	}

    	static void EnhancedForLoop()
    	{
    		var fibNumbers = new List<int>{0, 1, 1, 2, 3, 5, 8, 13};
    		int count = 0;
    		foreach(int number in fibNumbers)
    		{
    			count++;
    			Console.WriteLine($"Element #{count}: {number}");
    		}
    	}
        
        static void Main(string[] args)
        {
            SomeMathStuff();
            MinAndMaxValues();
            DoWhileLoop();
            EnhancedForLoop();

            var newValue = new NewClass();
            Console.WriteLine(newValue.GetValue());
        }
    }

}
