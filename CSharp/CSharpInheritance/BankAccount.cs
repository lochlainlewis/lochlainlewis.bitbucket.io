using System;
using System.Collections.Generic;

namespace CSharpInheritance
{
	public class BankAccount
	{
		private static int accountNumberSeed = 1234567890;
		private List<Transaction> allTransactions = new List<Transaction>();

		public string Number { get; }
		public string Owner { get; set; }
		public decimal Balance 
		{ 
			get
			{
				decimal balance = 0;
				foreach(var item in allTransactions)
				{
					balance += item.Amount;
				}
				return balance;
			}
		}

		public string Note
		{
			get
			{
				string note = "test";
				foreach(var item in allTransactions)
				{
					note = item.Notes;
				}
				return note;
			}
		}

		public override string ToString()
		{
			return Balance.ToString();
		}

		public BankAccount(string name, decimal initialBalance)
		{
			this.Number = accountNumberSeed.ToString();
			accountNumberSeed++;

			this.Owner = name;
			MakeDeposit(initialBalance, DateTime.Now, "Initial balance");
		}

		public void MakeDeposit(decimal amount, DateTime date, string note)
		{
			if(amount <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive!");
			}
			var deposit = new Transaction(amount, date, note);
			allTransactions.Add(deposit);
		}

		public void MakeWithDrawal(decimal amount, DateTime date, string note)
		{
			if(amount <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(amount), "Amount of withdrawl must be positive!");
			}
			if(Balance - amount < 0)
			{
				throw new InvalidOperationException("Insufficient funds for this transaction!");
			}
			var withdrawl= new Transaction(-amount, date, note);
			allTransactions.Add(withdrawl);
		}

	}
}