﻿using System;

namespace CSharpInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            var account = new BankAccount("Bob Sanders", 1000);
            Console.WriteLine($"Account {account.Number} was created for {account.Owner} with ${account.Balance} initial balance.");
            account.MakeWithDrawal(600, DateTime.Now, "Rent payment");
            Console.WriteLine($"{(account.Balance).ToString()}, for {account.Note}");
            account.MakeDeposit(100, DateTime.Now, "Fred paid me back");
            Console.WriteLine(account.Balance);
        }
    }
}
