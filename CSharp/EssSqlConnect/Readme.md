The SqlConnection application is a trivial program in C#.

Developed 02/19/2020 By Lochlain Lewis. 

This program is an alternative to using the Java program
for connecting to ESS1, retrieving sensor readings and 
uploading them to the database.

As of this version the data readings are hard-coded until
the port connections and data string retrieval code is
completed.

The database has been modified to include one table for
each sensor system. The first sensor system has seven 
columns for data and one id column.