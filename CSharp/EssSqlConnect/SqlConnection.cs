using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace EssSqlConnect
{
	class SqlConnection
	{
		// Access the database and put some data in it
		public void OpenDatabaseConnection()
		{
			// Before opening database get the actual data
			var tuple = GetDataBits();

			// Open the db 
			string cs = @"server=localhost;userid=root;password=Cat5paw!;database=essdb";
			using var con = new MySqlConnection(cs);
			con.Open();	

			// create the sql insert statement
			var sql = "INSERT INTO ess(datetime, pH, do2, ec, orp, air_temperature, water_temperature) VALUES(@datetime, @pH, @do2, @ec, @orp, @air_temperature, @water_temperature)";
			var cmd = new MySqlCommand(sql, con);
			cmd.Connection = con;

			Console.WriteLine("MySql connection open!");

			// Assign the data to columns in the table
			cmd.Parameters.AddWithValue("@datetime", tuple.Item1);
			cmd.Parameters.AddWithValue("@pH", tuple.Item2);
			cmd.Parameters.AddWithValue("@do2", tuple.Item3);
			cmd.Parameters.AddWithValue("@ec", tuple.Item4);
			cmd.Parameters.AddWithValue("@orp", tuple.Item5);
			cmd.Parameters.AddWithValue("@air_temperature", tuple.Item6);
			cmd.Parameters.AddWithValue("@water_temperature", tuple.Item7);

			// Send data to the database
			cmd.Prepare();
			cmd.ExecuteNonQuery();

			// Close database connection
			Console.WriteLine("Closing connection!");
			con.Close();
		}

		/* This function will connect to the sensor system and package the received data
		 * into a tuple for passing to the db connection function where it will 
		 * loaded in the database. */
		private Tuple<string, double, double, double, double, double, double> GetDataBits()
		{
			// hard coded until the sensor connection code is ready
			string datetime = "2019-03-09 07:54:45";
			double ph = 6.5;
			double do2 = 5.1;
			double ec = 114.05;
			double orp = 111.56;
			double airtemp = 15.03;
			double watertemp = 14.92;

			return new Tuple<string, double, double, double, double, double, double>(datetime, ph, do2, ec, orp, airtemp, watertemp); 
		}

	}

}