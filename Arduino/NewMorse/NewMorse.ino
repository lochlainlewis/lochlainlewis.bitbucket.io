/* NewMorse - for flashing pin 13 with SOS in Morse Code
 * Author: Lochlain Lewis Date: 04/03/2018
 * Source code created by:
 * David Mellis 20071102
 * 
 * Compare this program with the more conventional 
 * format in the OldMorse program. Or search for the 
 * originator's code to see a more basic representation.
 *
 * This application incorporates a <.h> file built by the
 * author of this version. The majority of the heavy lifting
 * code is incorporated in the <.h> file. 
 */

#include <Morse.h>

Morse morse(13);

void setup() 
{

}

void loop() 
{
  morse.dot(); morse.dot(); morse.dot();
  morse.dash(); morse.dash(); morse.dash();
  morse.dot(); morse.dot(); morse.dot();
  delay(3000);
}
