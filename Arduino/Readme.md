Where Arduino programs consist of only one file information 
normally found in the readme file is in the program file as
a comment at the top of the file. Where Arduino files 
incorporate multiple files there is a separate readme.