#include <SoftwareSerial.h>

SoftwareSerial XBee(2, 3);

const int pinSix = 6;  //Blue
const int pinFive = 5; //Green
const int pinFour = 4; //Red

void setup() 
{
  Serial.begin(9600);
  XBee.begin(9600);
  blinkLED(6, 3);
//  blinkLED(5, 3);
//  blinkLED(4, 3);
}

void loop() 
{
  if(Serial.available() > 0)
  {
    int incomingByte = Serial.read();
    if(incomingByte)
    {
      switch(incomingByte)
      {
        case 'F': // Forward
          sendSignal(incomingByte);
          break;
   case 'B':  // Backward
          sendSignal(incomingByte);
          break;
        case 'R':  // Right
          sendSignal(incomingByte);
          break;
        case 'L':  // Left
          sendSignal(incomingByte);
          break;
        case 'S':  // Stop
          sendSignal(incomingByte);
          break;
        case 13:
          Serial.println("End command");
          break;
        default:
          Serial.println("Not a valid command.\nPlease enter a valid command.");
          blinkLED(4, 1);
        break;
      }
    }
  }
  delay(2000);
}

void sendSignal(int byteToSend)
{
  XBee.write(byteToSend);
  Serial.println("Signal sent.");
  blinkLED(5, 2);
}

void blinkLED(int pinNumber, int iterations)
{
  for(int i = 0; i < iterations; i++)
  {
    digitalWrite(pinNumber, HIGH);
    delay(500);
    digitalWrite(pinNumber, LOW);
    delay(500);
  }
}