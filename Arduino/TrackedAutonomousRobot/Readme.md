Author: Lochlain Lewis  Date: 09/12/2017

This is Arduino code for an autonomous tracked vehicle that incorporates ultra sonic sensors.
The code is in two parts. The Controller.io file contains code to run with the Arduino IDE 
from the user's computer. The purpose of this code is to input commands to the vehicle via
an RF module. The CrawlerV02.io file contains code to load on the vehicle's computer. This 
latter code receives user input commands from the computer via the onboard RF module.

The RF module is capable of transmitting to distances of 1 mile (LOS) when the transmitter and
receiver modules are approximately 1 meter off the ground. Distance testing with the 
receiver on the vehicle has not been accomplished as of this version. Local testing up to nominal
distances of approx. 10 meters indoors and out are successful. The RF modules have not been 
modified and incorporate an onboard flexible antenna. 

As of this version incorporation of the sensors and command injection/override of the sensor
commands have not been tested. This version incorporates and is intended for only command
transmit/receive/react testing. 