/** Author: Lochlain Lewis Date: 9/11/2017
 * This program tests the reaction time of 
 * the ultra-sonic sensor. When the forward
 * speed of the robot is too high the reaction
 * of the adafruit ultrasonic sensor causes the
 * robot to contact the obstacle. To fix the 
 * issue without changing sensors a curve must
 * be developed that allows the robot to change
 * the minimum distance to stop. This curve will
 * allow the robot to adjust for varing speeds 
 * while not abversely impacting performance and
 * mission success. 
 */
 
const int trigPin = 7;
const int echoPin = 6;

// defines variables
long duration;
int distance;
void setup() 
{
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication
}
void loop() 
{
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  
  // Calculating the distance
  distance= duration*0.034/2;
  
  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance);
  //pulseIn(echoPin, LOW);
}
