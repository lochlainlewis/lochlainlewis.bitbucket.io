#include <Wire.h>
#include <SoftwareSerial.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *LRM = AFMS.getMotor(1);
Adafruit_DCMotor *RRM = AFMS.getMotor(2);

SoftwareSerial XBee(2, 3);  // XBee comms pins (RX, TX)

long duration;
int distance;
int lookieLoo;
const int trigPin = 7;
const int echoPin = 6;
int greenLED = 4;
long previousMillis = 0;

void setup() 
{
  XBee.begin(9600);
  //set ultrasonic sensor pins
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(greenLED, OUTPUT);
  
  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  LRM->setSpeed(150);
  LRM->run(FORWARD);
  // turn on motor
  LRM->run(RELEASE);
  
  RRM->setSpeed(150);
  RRM->run(FORWARD);
  // turn on motor
  RRM->run(RELEASE);
  Serial.begin(9600);
}

void loop()
{
  /*if(XBee.available() > 0)
  {
    int incomingByte = XBee.read();
    digitalWrite(greenLED, HIGH);
    delay(500);
    digitalWrite(greenLED, LOW);
    delay(500);
    executeCommand(incomingByte);
  }*/

  if(XBee.available())
  {
    byte temp = XBee.read();
    executeCommand(temp);
  }
}
/*********END MAIN LOOP********************/
void executeCommand(int cmd)
{
  digitalWrite(greenLED, HIGH);
  delay(250);
  digitalWrite(greenLED, LOW);
  delay(250);  
  switch(cmd)
  {
    case 'F':
      forward();
      break;
    case 'B':
      reverse();
      break;
    case 'R':
      turnRight90();
      break;
    case 'L':
      turnLeft90();
      break;
    case 'S':
      stop();
      break;
    default:
      break;
  }
}
void stop()
{
  
  LRM->run(RELEASE);
  RRM->run(RELEASE);
  
}
/*********FORWARD**************************/
void forward()
{

  LRM->setSpeed(150);
  RRM->setSpeed(150);
  LRM->run(FORWARD);
  RRM->run(FORWARD);
 
}
/*********END FORWARD********************/
/********* REVERSE***********************/
void reverse()
{
  LRM->run(BACKWARD);
  RRM->run(BACKWARD);

  for (int i = 0; i < 255; i++) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
 
    delay(10);
  }
  for (int i = 255; i != 0; i--) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
  
    delay(10);
  }  
}
/*********END REVERSE********************/
/*********TURN RIGHT* 360****************/
void turnRight360()
{
  LRM->run(FORWARD);
  RRM->run(BACKWARD);

  for (int i = 0; i < 255; i++) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
 
    delay(10);
  }
  for (int i = 255; i != 0; i--) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
  
    delay(10);
  }
}
/*********END TURN RIGHT 360***********/
/*********TURN LEFT 360****************/
void turnLeft360()
{
  LRM->run(BACKWARD);
  RRM->run(FORWARD);

  for (int i = 0; i < 255; i++) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
 
    delay(10);
  }
  for (int i = 255; i != 0; i--) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
  
    delay(10);
  }
}
/*********END TURN LEFT 360************/
/*********TURN LEFT 90*****************/
void turnRight90()
{
  LRM->run(FORWARD);
  RRM->run(BACKWARD);

  for (int i = 0; i < 150; i++) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
 
    delay(10);
  }
  for (int i = 150; i != 0; i--) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);
  
    delay(10);
  }
}
/*********END TURN LEFT 90**************/
/*********TURN RIGHT 90*****************/
void turnLeft90()
{
  LRM->run(BACKWARD);
  RRM->run(FORWARD);

  for (int i = 0; i < 150; i++) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i); 
    delay(10);
  }
  for (int i = 150; i != 0; i--) 
  {
    LRM->setSpeed(i); 
    RRM->setSpeed(i);  
    delay(10);
  }
  return;
}
/*********END TURN RIGHT 90**************/
/*********PING DISTANCE*****************/
int pingDistance()
{
  // clear trigPin
  distance = 0;
  digitalWrite(trigPin, LOW);
  delay(0.002);

  digitalWrite(trigPin, HIGH);
  delay(0.01);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = duration * 0.034/2;
  pulseIn(echoPin, LOW);
  duration = 0;

  Serial.print("Distance: ");
  Serial.println(distance);

  return distance;
}
/*********END PING DISTANCE********************/
/* if interval = 500, currentMillis = 5000
 * and previousMillis = 4750 then the first pass*/
//void blinkLED(int interval)
//{
//  unsigned long currentMillis = millis();
//
//  if(currentMillis - previousMillis > interval)
//  {
//    previousMillis = currentMillis;
//    if(ledState == LOW)
//    {
//      ledState == HIGH;
//    }
//    else
//    {
//      ledState = LOW;
//    }
//    digitalWrite(ledPin, ledState);
//  }
//}