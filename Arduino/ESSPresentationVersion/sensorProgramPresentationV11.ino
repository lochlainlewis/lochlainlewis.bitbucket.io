/* Sensor Server program for Arduino Uno
 * Source code by: Mikael Kindborg, Evothings AB
 * Date: 10/20/2013
 * 
 * Modified by Lochlain Lewis
 * Date 12/15/2017 
 * This application receives sensor readings directly from 
 * the Environmental Sensor System (ESS) via the USB connection.
 * It is intended for presentation purposes only, and only for
 * use as a mobile configuration for demonstrating the ESS to
 * stakeholders. This is a stand-alone application and does not
 * require the communication or database class files.
 * 
 * Modified by Lochlain Lewis
 * Date 04/12/2018
 * 
 * Mod 0: updated to WiFi101 libraries, added character string
 * <char data[]> as a hard-coded example of target string for 
 * transmission to server heretofore called the client. 
 * The goal of MOD1 is develop the program that will enable server
 * like characteristics on the Arduino, to be polled by the client -
 * the client is a stand alone server running in the lab.
 * MOD0 successfully allows the client to poll the server (the Arduino) 
 * which then transmitts the dummy string to the client for storage.
 * 
 * Mod 1: Upgrades/improvements to MOD1; 
 * -timing established on both the server and the client such that
 *  the server is not continuously holding the socket open.
 *  
 * Mod 2: Incorporates pH, DO2, Conductivity, ORP sensors.
 * Moves temperature probe to the Arduino
 */
#include <SPI.h>
#include <Wire.h>
#include "RTClib.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 2
#define TOTAL_CIRCUITS 4

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature tempSensor(&oneWire);
RTC_DS1307 RTC;

int channel_ids[] = {99, 97, 98, 100};
char *channel_names[] = {"pH", "DO2", "ORP", "EC"};

uint8_t yearval;
uint8_t monthval;
uint8_t dayval;
uint8_t hourval;
uint8_t minuteval;
uint8_t secondval;

char *colon = ":";
char *doubleColon = "::";
char *slash = "/";
char *comma = ", ";
char *space = " ";
char *zero = "0";
char *currentTime = "Current time: ";
char *tab = "\t";
char sensordata[30];
char endOfFile = '\0';
byte sensor_bytes_received = 0;   
int input = 0;
byte code = 0;
byte in_char = 0;
char xmitString[90];
char timeString[59];
float temp;

void setup() 
{
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  tempSensor.begin();
}

void loop() 
{
  char finalTemp[12] = "Temp ";
  char tempString[7];
  delay(5000);
  printDateTime();
  getSensorData();
  tempSensor.requestTemperatures();
  temp = tempSensor.getTempCByIndex(0);
  dtostrf(temp, 6, 2, tempString);
  strcat(finalTemp, tempString);
  strcat(xmitString, comma);
  strcat(xmitString, finalTemp); 
  Serial.println(xmitString); 
}

/**************** PRINT DATE AND TIME *****************/
void printDateTime()
{
  DateTime newTime = RTC.now();
  yearval = newTime.year();
  monthval = newTime.month();
  dayval = newTime.day();
  hourval = newTime.hour();
  minuteval = newTime.minute();
  secondval = newTime.second();
  strcat(timeString, yearval);
  strcat(timeString, colon);
  strcat(timeString, monthval);
  strcat(timeString, colon);  
  strcat(timeString, dayval);
  strcat(timeString, doubleColon);
  strcat(timeString, hourval);
  strcat(timeString, colon);
  strcat(timeString, minuteval);
  strcat(timeString, colon);
  strcat(timeString, secondval);
  strcat(timeString, colon);  
  //Serial.print(timeString);  
//  Serial.print(newTime.year(), DEC);
//  Serial.print('/');
//  Serial.print(newTime.month(), DEC);
//  Serial.print('/');
//  Serial.print(newTime.day(), DEC);
//  Serial.print(' ');
//  Serial.print(newTime.hour() - 1, DEC); // Setup during daylight saving
//                                     // -1 adjusted for end of daylight saving
//  Serial.print(':');
//  Serial.print(newTime.minute(), DEC);
//  Serial.print(':');
//  Serial.print(newTime.second(), DEC);
//  Serial.print('\t');
}

/***************** GET SENSOR DATA ******************************/
String getSensorData()
{
      memset(xmitString, 0, sizeof(xmitString));
      // loop through all the sensors
      for (int channel = 0; channel < TOTAL_CIRCUITS; channel++) 
      {       
        // call the circuit by its ID number.
        Wire.beginTransmission(channel_ids[channel]);
        // request a reading by sending 'r' 
        Wire.write('r');
        // end the I2C data transmission.                          
        Wire.endTransmission();                   
        // AS circuits need a 1 second before the reading is ready
        delay(1000);  
        // reset data counter
        sensor_bytes_received = 0;
        // clear sensordata array;                        
        memset(sensordata, 0, sizeof(sensordata));        

        // call the circuit and request 48 bytes (this is more then we need).
        Wire.requestFrom(channel_ids[channel], 48, 1);    
        code = Wire.read();

        // are there bytes to receive?
        while (Wire.available()) 
        { 
          // read a byte.       
          in_char = Wire.read();            
          
          /*  null character indicates end of command
           *  end the I2C xmit
           *  exit while loop
           */
          if (in_char == 0) 
          {               
            Wire.endTransmission();         
            break;                          
          }
          else 
          {
            // append this byte to the sensor data array.
            sensordata[sensor_bytes_received] = in_char;      
            sensor_bytes_received++;
          }
        }  // End While Loop
        
        /*  concatenate xmit string with new channel data 
         *  and print data to the lcd screen
         */
        strcat(xmitString, channel_names[channel]);
        strcat(xmitString, colon);
        
        // switch case based on what the response code is.
        switch (code) 
        {  
          // decimal 1  means the command was successful.                     
          case 1:            
            strcat(xmitString, sensordata);  

            if(channel < TOTAL_CIRCUITS - 1)
            {
              strcat(xmitString, comma);
            }
            break;
            
          // decimal 2 means the command has failed.
          case 2:                             
            delay(1000);
            break;
            
          // decimal 254 command has not yet been finished calculating.
          case 254:   
            delay(1000);
            break;                              
            
          // decimal 255 means there is no further data to send.
          case 255:                       
            delay(1000);
            break;                      
        } // switch statement    
      } // for loop
 }
