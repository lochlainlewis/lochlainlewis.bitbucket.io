import React from "react"
import Layout from "../components/layout"
export default() => (
	<Layout>
		<h1>Hi! I'm a geek. I'm building this fake Gatsby site for the heck of it!</h1>
		<p>
			What are you wearing right now? No, don't tell me. That question was rhetorical.
			I don't really want to know.
		</p>
	</Layout>
)
