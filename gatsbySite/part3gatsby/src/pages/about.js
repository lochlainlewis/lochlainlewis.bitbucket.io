import React from "react"
import Layout from "../components/layout"
export default() => (
	<Layout>
		<h1>About me</h1>
		<p>I'm a recent graduate of the PSU graduate Software Engineering program.
		And yes, I'm looking for challenging work in software development. My interests 
		center around machine learning, AI, and robotics and sensors.</p>
	</Layout>
)