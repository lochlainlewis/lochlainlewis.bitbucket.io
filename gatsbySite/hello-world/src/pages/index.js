import React from "react"
import Header from "../components/header"
import { Link } from "gatsby"

export default () => (
	<div style={{ color: 'purple' }}>
		<Link to="/contact/">Contact</Link>
		<div>
			<Link to="/about/">About</Link>
		</div>
		<Header headerText="Hello Gatsby!" />
		<p>Welcome to my world.</p>
		<img src="https://source.unsplash.com/random/400X200" alt="" />
	</div>
)
