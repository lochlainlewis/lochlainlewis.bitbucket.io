//
//  ViewController.swift
//  UINumberUno
//
//  Created by Lochlain Lewis on 1/7/20.
//  Copyright © 2020 Lochlain Lewis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var leftimageview: UIImageView!
    
    @IBOutlet weak var rightImageView: UIImageView!
    
    @IBOutlet weak var leftscorelabel: UILabel!
    
    @IBOutlet weak var rightscorelabel: UILabel!
    
    var leftscore = 0
    var rightscore = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func dealtapped(_ sender: Any) {
        let leftnumber = Int.random(in:2...14)
        let rightnumber = Int.random(in:2...14)
        
        leftimageview.image = UIImage(named:"card\(leftnumber)")
        rightImageView.image = UIImage(named:"card\(rightnumber)")
        
        if leftnumber > rightnumber {
            //leftside wins
            leftscore += 1
            leftscorelabel.text = String(leftscore)
        }
        else if rightnumber > leftnumber {
            //rightside wins
            rightscore += 1
            rightscorelabel.text = String(rightscore)
        }
        else {
            // tie
        }
    }
    
    
}

