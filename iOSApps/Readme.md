These are pretty trivial iOS apps developed from a few tutorial sites
including the Apple tutorial: 

https://developer.apple.com/library/archive/referencelibrary/GettingStarted/DevelopiOSAppsSwift/index.html#//apple_ref/doc/uid/TP40015214-CH2-SW1

These applications are built with the Swift language in the the xCode IDE.

All of the apps are locally deployable by loading the code into xCode or some other 
development environment capable of developing iOS apps. 