This is an on-going project to learn Software Define Radio (SDR).

I began this project after getting laid off from the YMCA - just
another static of COVID-19.

A door closes and another opens. Since the 'lockdown' I've been
spending time learing how to receive signals. It was a challenge just getting 
the software to load in a functional manner. I'm using a Macbook Pro
from early 2015 with OS 10.15 Catalina loaded on it, 
the GNU Radio software and the GRC.

The work around turned out to be an application for the GRC. I'm fairly
certain it's missing libraries. But it seems to working so far.

The goal is to learn to receive and transmit signals with SDR using
a HackRF One. And eventually learn the underlying Python Code so I can
write my own SDR radio programs.