The link below is for the video I made of my range of motion and
limits test I conducted shortly after finishing construction of
the open source KickStarter project. Not my project, but I did 
support it with the purchase of two of their robot kits. My 
intention is to deep dive into AI and machine learning.

https://youtu.be/0szbYHV7SUk

