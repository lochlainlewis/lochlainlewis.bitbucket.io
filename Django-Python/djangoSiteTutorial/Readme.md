This is my first solo Django-Python webapp. While still in grad school I 
worked on a team project where one of my classmates was very skilled with
Django. Having a background in Python is helpful when working with Django
but it's not really necessary if you're going to stick with the project 
guidelines. I have some experience with HTML, CSS, JSON, and Python so I
did go a little bit beyond the scope of the tutorial. 

Django is a very powerful framework for Python. Following the tutorial at

https://docs.djangoproject.com/en/2.2/intro/tutorial01/

is a good way to get started with Django! But beware, there is a newer
version of Django (2.2) and an older version of the tutorial that is 
focused on legacy Django versions. 

I also posted a Youtube video of the functionality of the finished product 
and some of the code developed while going through the tutorial. 
Keep in mind, this video is not a how-to. For that, just go to the source 
and read the tutorial. It's actually an easy read and even easier if you 
decide to write the code as you go.

https://www.youtube.com/watch?v=6YjD7tYLx84

