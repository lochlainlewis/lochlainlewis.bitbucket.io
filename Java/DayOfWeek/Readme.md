DayOfWeek Application

Author: Lochlain Lewis
Date:   2019.09.11

This application calculates the day of the week of a target date that the user
inputs into the program. The application also takes the current date and day as 
input to use as arguments in the process. The application does not make use of 
any Java Calendar or Date/Time API in its calculations. Nor does it make use of 
a lot of if statements in determining what to do about leap years. Rather the 
application uses an algorithm devised by the author to compute the number of 
leap years to account for in the calculations. 

LIMITATIONS - The application is only accurate for dates after January 1, 1753.
A quick search of a 1753 calendar reveals that apparently, 11 days were missing 
from some, if not all, 1752 calendars. Furthermore, the acceptance of the Gregorian
calendar seems questionable the further back in time one searches. This latter point 
is important when attempting to verify the accuracy of the program. Calendars 
before October 1752 might not be accurate depending on which type, the Julian or 
the Gregorian calendar.

The application will not work going forward. So it will not accurately inform the
user of a day of some future date. This limitation will be addressed with the next
refactoring.