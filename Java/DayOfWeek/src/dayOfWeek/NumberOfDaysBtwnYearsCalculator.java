/** Calculates the number of days from the target year
 *  to the current year inclusive of leap years and without
 *  the use of a lot of <if> statements. */
package dayOfWeek;

public class NumberOfDaysBtwnYearsCalculator 
{
	public int calculateNumberOfLeapYears(int startYr, int targetYr)
	{
		int leapYrs = 0;
		int regYrs = 0;
		int totalDays = 0;
				
		leapYrs = (((targetYr - startYr) / 100) * 24) + ((targetYr - startYr) % 100) / 4;
		regYrs = calculateNumberOfRegYears(startYr, targetYr, leapYrs);
		totalDays = calculateTotalDays(regYrs, leapYrs);
		
		return totalDays;
	}
	
	public int calculateNumberOfRegYears(int startYr, int targetYr, int leapYrs)
	{
		int regYrs = 0;
		regYrs = (targetYr - startYr) - leapYrs;
		return regYrs;
	}
	
	public int calculateTotalDays(int numberRegYrs, int numberLeapYrs)
	{
		int totalDays = 0;
		totalDays = (numberRegYrs * 365) + (numberLeapYrs * 366);
		return totalDays;
	}
}
