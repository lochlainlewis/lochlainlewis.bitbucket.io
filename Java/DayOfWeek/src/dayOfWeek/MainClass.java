/** Author: Lochlain Lewis  Date: 20190911
 *  
 *  This application returns the day of the week of a user
 *  defined target date. The application utilizes the current
 *  date and day and using the algorithm in the
 *  NumberOfDaysBtwnYearsCalculator class it calculates the
 *  day of the week without the use of any Java Calendar APIs
 *  and without the need of a lot of if statements. 
 *  
 *  Please see the Readme.md file for more information about 
 *  the limitations of this application.
 * ********************************************************* */

package dayOfWeek;

import java.util.ArrayList;

public class MainClass
{
	static int newTodayDate = 0;
	static int newTodayMonth = 0;
	static int newTodayYear = 0;
	static String newTodayDay = "";
	
	public static void main(String[] args) 
	{
		UserDateCollector newTargetDate = new UserDateCollector();
		NumberOfDaysBtwnYearsCalculator daysBtwnCurrentAndTargetYrs = new NumberOfDaysBtwnYearsCalculator();
		JulianDateCalculator julianNumber = new JulianDateCalculator();
		DayOfWeekCalculator newDayOfWeek = new DayOfWeekCalculator();
		
		int numberOfDaysBtwnYears = 0;
		int newDayShiftNumber = 0;
		int[] newJulianNumbers = new int[2];
		String targetDateDay = "";
		
		/** get the user input; today's date and target date */
		ArrayList<TodayDay> todayDate = newTargetDate.getCurrentDate();
		int[] targetDate = newTargetDate.getTargetDate();
		
		/** Target date */
		int targetYear = targetDate[0];
		int targetMonth = targetDate[1];
		int targetDay = targetDate[2];
		
		/** Today's date */
		todayDate.forEach(day -> {
			newTodayDay = day.getDay();
			newTodayYear = day.getYear();
			newTodayMonth = day.getMonth();
			newTodayDate = day.getDate();	
		});
				
		// Number of days from current year to target year
        numberOfDaysBtwnYears = daysBtwnCurrentAndTargetYrs.calculateNumberOfLeapYears(targetYear, newTodayYear);
        
        // Number of days from January 1 of target year to target date
        newJulianNumbers = julianNumber.getDaysBtwnJanONEAndTargetDate(newTodayMonth, newTodayDate, targetMonth, targetDay);
        
        // Number to subtract from current day to get target date day
        newDayShiftNumber = (numberOfDaysBtwnYears - newJulianNumbers[1] + newJulianNumbers[0]) % 7;
        
        // Pass newDayShiftNumber and current day to get day of target date
        targetDateDay = newDayOfWeek.getDayOfWeek(newDayShiftNumber, newTodayDay);
        
        System.out.println(targetDate[1] + "/" + targetDate[2] + "/" + targetDate[0] 
        		+ " was a " + targetDateDay);
		
	}
}
