package dayOfWeek;

public class DateChecker 
{
	// to inspect user input for accuracy
	public boolean checkDate(int[] newDate)
	{
		boolean[] results = new boolean[3];
		boolean finalResult = false;
		
		if(newDate[0] > 0)
			results[0] = true;
		if(newDate[1] < 13)
			results[1] = true;
		if(newDate[2] < 32)
			results[2] = true;
		for(int i = 0; i < 3; i++)
			if(!(results[i] == true))
			{
				finalResult = false;
				return finalResult;
			}
			else
				finalResult = true;
		
		return finalResult;
	}	
}
