package dayOfWeek;

import java.time.LocalDate;

public class CheaterDayDateCalculator 
{
	public void easyCalculator(int yr, int month, int date)
	{
        LocalDate localDate = LocalDate.of(yr, month, date);
        java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        System.out.println(localDate + " was a " + dayOfWeek);
	}
}
