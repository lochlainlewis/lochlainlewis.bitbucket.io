package dayOfWeek;

public class DayOfWeekCalculator 
{
	public String getDayOfWeek(int count, String day)
	{

		String dayOfWeek = "";
		int today = -1;
		int dayIndex = -1;
		
		day = day.toLowerCase();
		
		switch(day)
		{
		case "sunday":
			today = 0;
			break;
		case "monday":
			today = 1;
			break;
		case "tuesday":
			today = 2;
			break;
		case "wednesday":
			today = 3;
			break;
		case "thursday":
			today = 4;
			break;
		case "friday":
			today = 5;
			break;
		case "saturday":
			today = 6;
			break;
		default:
			System.out.println("Error at day switch.");
		}
		
		if(today < count)
		{
			dayIndex = 7 - (count - today);
		}
		else
		{
			dayIndex = today - count;
		}
		
		switch(dayIndex)
		{
		case 0:
			dayOfWeek = "Sunday";
			break;
		case 1:
			dayOfWeek = "Monday";
			break;
		case 2:
			dayOfWeek = "Tuesday";
			break;
		case 3:
			dayOfWeek = "Wednesday";
			break;
		case 4:
			dayOfWeek = "Thursday";
			break;
		case 5:
			dayOfWeek = "Friday";
			break;
		case 6:
			dayOfWeek = "Saturday";
			break;
		default:
			dayOfWeek = "Error at dayIndex switch.";	
		}
		
		return dayOfWeek;
	}
}
