/** Calculates the number of days between Jan 1 of current year
 *  and the current date (in the getDaysBtwnJanONECurrentYrAndToday
 *  method) and the number of days between Jan 1 of the target 
 *  year and the target date (in the getDaysBtwnJanONEAndTargetDate
 *  method).
 *  */

package dayOfWeek;

public class JulianDateCalculator 
{
	public int[] getDaysBtwnJanONEAndTargetDate(int todayMonth, int todayDate, int targetMonth, 
													int targetDate)
	{
		int[] julianValues = new int[2];
		int julianForTargetYr = 0;
		
		int[] daysInMonthArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		for(int i = 0; i < targetMonth - 1; i++)
		{
			julianForTargetYr += daysInMonthArray[i];
		}
			
		julianForTargetYr += targetDate;
		
		julianValues[1] = julianForTargetYr;
		
		julianValues[0] = getDaysBtwnJanONECurrentYrAndToday(todayDate, todayMonth);
		
		
			
		return julianValues;
	}
	
	public int getDaysBtwnJanONECurrentYrAndToday(int todayDay, int todayMonth)
	{

		int numberDaysFromJanONEtoToday = 0;
		
		int[] daysInMonthArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		for(int i = 0; i < todayMonth - 1; i++)
		{
			numberDaysFromJanONEtoToday += daysInMonthArray[i];
		}
		
		numberDaysFromJanONEtoToday += todayDay;
		
		return numberDaysFromJanONEtoToday;
	}
}
