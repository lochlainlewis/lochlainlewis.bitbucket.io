package dayOfWeek;

public class TodayDay 
{
	private String today;
	private int year, month, date;
	
	public TodayDay(String newDay, int newYear, int newMonth, int newDate)
	{
		this.today = newDay;
		this.year = newYear;
		this.month = newMonth;
		this.date = newDate;
	}
	
	public String getDay()
	{
		return today;
	}
	
	public void setDay(String newDay)
	{
		this.today = newDay;
	}
	
	public int getYear()
	{
		return year;
	}
	
	public void setYear(String newYear)
	{
		this.today = newYear;
	}
	
	public int getMonth()
	{
		return month;
	}
	
	public void setMonth(String newMonth)
	{
		this.today = newMonth;
	}
	
	public int getDate()
	{
		return date;
	}
	
	public void setDate(String newDate)
	{
		this.today = newDate;
	}
}
