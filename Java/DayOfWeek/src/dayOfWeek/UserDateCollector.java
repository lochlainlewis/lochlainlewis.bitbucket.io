package dayOfWeek;

import java.util.ArrayList;
import java.util.Scanner;

public class UserDateCollector 
{
	
    public ArrayList<TodayDay> getCurrentDate() 
    {	
    	Scanner dayScan = new Scanner(System.in);
    	ArrayList<TodayDay> newDay = new ArrayList<>();
    	
    	int [] numbers = new int[3];
    	String today = "";
    	
    	System.out.println("Enter what day it is today.");
    	today = dayScan.nextLine();
    	
    	Scanner numScanner = new Scanner(System.in);		
        System.out.println("Enter today's date using all numbers, in the following order: \n"
        			+ "First enter the year, then month, then date.");
            
        for (int i = 0; i < 3; i++) 
        {
        	if (numScanner.hasNextInt()) 
        	{
        		numbers[i] = numScanner.nextInt();
            }
        	else 
            {
        		System.out.println("You didn't enter the date correctly.");
                break;
            }
        }
        
        newDay.add(new TodayDay(today, numbers[0], numbers[1], numbers[2]));
        
        return newDay;
    }
    
    public int[] getTargetDate()
    {
    	Scanner numScanner = new Scanner(System.in);
    	
    	int [] numbers = new int[3];
    	    		
        System.out.println("Enter a target date using all numbers, in the following order: \n"
        			+ "First enter the year, then month, then date.");
    
            
        for (int i = 0; i < 3; i++) 
        {
        	if (numScanner.hasNextInt()) 
        	{
        		numbers[i] = numScanner.nextInt();
            } 
        	else 
            {
        		System.out.println("You didn't enter the date correctly.");
                break;
            }
        }
        return numbers;
    }
}
