package TestForms;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class DateAndTimeTest 
{

	public static void main(String[] args) 
	{
		String newDate = "";
		String newYear = "";
		String yesterdayDate = "";
		String tomorrowDate = "";
		
		LocalDate localDate = LocalDate.now();
		System.out.println("LocalDate.now() as a LocalDate object: " + localDate);
		
		newDate = LocalDate.of(1941, 12, 07).toString();
		System.out.println("LocalDate.now() as a String: " + localDate);
		
		newYear = newDate.substring(0, 4);
		System.out.println("Grabbing the year out of the string: " + newYear);
		
		yesterdayDate = LocalDate.now().minusDays(1).toString();
		System.out.println("LocalDate minus 1 to equal yesterday: " + yesterdayDate);
		
		tomorrowDate = LocalDate.now().plusDays(1).toString();
		System.out.println(tomorrowDate);
		
		System.out.println("Subtracting the number of days between current day as of \n"
							+ "September 7, 2019");
		yesterdayDate = LocalDate.now().minusDays((97154 + (250 - 1))).toString();
		DayOfWeek newDayOfWeek = LocalDate.parse(yesterdayDate).getDayOfWeek();
		System.out.println(newDayOfWeek + ", " + yesterdayDate);
		
	}

}
