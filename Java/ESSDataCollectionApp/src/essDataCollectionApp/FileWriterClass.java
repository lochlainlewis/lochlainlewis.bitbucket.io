package essDataCollectionApp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterClass 
{
	/* path to the data file */
	public static final String MACFILENAME = "/Users/lochlainlewis/Documents/testFiles/essDataFile.csv";
	public static final String MSFILENAME = "C:\\Users\\LochPC\\Documents\\ESSData\\essDataFile.csv";
	public static final String LINUXFILENAME = "/home/pi/Documents/ESSData/essDataFile.csv";
	
	public void writeToMacFile(String dataString, String osType) throws IOException
	{
		FileWriter fileWriter = null;
		BufferedWriter out = null;
		//System.out.println(dataString);
		try
		{
			/* initialize the FileWriter and BufferedWriter objects */
			if(osType.compareTo("mac") == 0)
			{
				fileWriter = new FileWriter(MACFILENAME, true);
				
			}
			else if(osType.compareTo("ms")== 0)
			{
				fileWriter = new FileWriter(MSFILENAME, true);
			}
			else if(osType.compareTo("linux") == 0)
			{
				fileWriter = new FileWriter(LINUXFILENAME, true);
			}
			
			out = new BufferedWriter(fileWriter);
			
			/* write data string to file */
			out.write(dataString);
			out.newLine();
			out.close();
			//System.out.println("Data transfer complete.");
		}
		catch(IOException e)
		{
			//System.out.println("Exception occurred!");
		}		
	}
}
