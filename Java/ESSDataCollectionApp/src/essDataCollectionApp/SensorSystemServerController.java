/* Author: Lochlain Lewis Date: 2018.12.27
 * 
 * This is an improvement and modification of the original version as follows.
 * 
 * This version adds Linux specific formatting for saving data to the file system.
 * Where previous versions were specific to Windows and Unix. The next version will
 * include a user facing GUI to select the platform this application is running on. 
 * 
 * The application consists of two classes, the SensorSystemServerController and the 
 * FileWriterClass. The former contains the main method and will be referred to as
 * the main class. The main class triggers the sensor system via a WiFi connection.
 * The sensor system responds with data readings. The main class passes the string to 
 * the FileWriterClass where the string is saved in a database. At the time of this writing 
 * the database is a simple csv file. The next version the file writer will pass the 
 * data string to a MySQL database.
 * 
 */

package essDataCollectionApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SensorSystemServerController
{
	public static void main(String[] args) throws UnknownHostException, IOException
	{
		//System.out.println("S3C Version V.1.0.1");
		FileWriterClass newDataString = new FileWriterClass();
		DateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date newDateTime = new Date();
		String currentTime = dateTime.format(newDateTime);
		
		/* these three lines determine for which OS the FileWriterClass will 
		 * format the file write process. */
		
		//String systemTypeMicrosoft = "ms";
		//String systemTypeMac = "mac";
		String systemLinux = "linux";
		
		String hostName = "10.0.0.185";
		int portNumber = 8001;
		String initiateKey = "1";
		boolean essSocketClosed = true;
		try
		(
				/* open a socket */
				Socket essSocket = new Socket(hostName, portNumber);
				PrintWriter out = new PrintWriter(essSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(
						new InputStreamReader(essSocket.getInputStream()));
				)
		{
//			BufferedReader stdIn = new BufferedReader(
//					new InputStreamReader(System.in));
			String fromServer;
			//System.out.println("Initiating data collection.");
			
			/* send the trigger signal to initiate data collection */
			out.println(initiateKey);
			
			/* read the incoming data string*/
			while((fromServer = in.readLine()) != null)
			{
				//System.out.println("Sending data to file.");
				
				/* send to the data string to the FileWriter class */
				/* but first put the date/time string at the front */
				fromServer = currentTime + ", " + fromServer;
				newDataString.writeToMacFile(fromServer, systemLinux);
				essSocket.close();
				essSocketClosed = false;
			}
		}
		catch(SocketException e)
		{
			if(essSocketClosed)
			{
				//System.out.println("Socket Exception thrown.");
			}
		}
		catch(IOException e)
		{
			//System.out.println("I/O Exception thrown.");
		}
		
	}
}
