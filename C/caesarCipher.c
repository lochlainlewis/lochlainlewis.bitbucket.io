#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

/* The function does not take an argument however it 
 * returns the value <codeKey> used to determine the shift
 * for encoding the message. The value is the number of spaces
 * to shift in the encoding process; so if the codeKey value is
 * 2, A will become C. 
 */
int getCodeKey() {
	int r = 0;
	int codeKey = 0;

	srand((unsigned)time(NULL));
	r = rand();
	codeKey = (r % 20) + 1;
	printf("The codeKey is: %d\n", codeKey);

	return codeKey;
}

/* This function takes the string inputStr as an argument and 
 * converts each char to the encoded char using the codeKey from
 * getCodeKey func. It returns the completed encoded string.
 */
void getCode(char *inputStr) {
	int newAscii = 0;
	int asciiAtPosit = 0;
	int codeKey = getCodeKey();
	int shiftPos = 90 - codeKey;
	char outputStr;
	printf("From getCode: \n%s\n", inputStr);

	for(int i = 0; inputStr[i] != '\0'; i++) {
		asciiAtPosit = (inputStr[i]);
		newAscii = asciiAtPosit + codeKey;

		if(asciiAtPosit > shiftPos) {
			newAscii = (codeKey - (90 - asciiAtPosit)) + 64;
		}
		if(asciiAtPosit <= shiftPos) {
			newAscii = asciiAtPosit + codeKey;
		}
		if(asciiAtPosit == 32) {
			newAscii = 32;
		}

		
		//strcat(newAscii, &outputStr);
		//printf("%c\n", newAscii);
		//printf("%c = %d\t%d\n", inputStr[i], inputStr[i], newAscii);
		printf("%c = %c\n", inputStr[i], newAscii);
		// for(i = 0; inputStr[i] != '\0'; i++) {
		// 	outputStr[i] = ("%c", newAscii);
		// }
	}


	return;
}

int main() {
	char buf[BUFSIZ];
	char buf2[BUFSIZ];
	int i = 0;
	int inputVal = 0;
	char *p;

	printf("Enter a phrase to encoded:\n");

	if(fgets(buf, sizeof(buf), stdin) != NULL) {
		printf("Thank you! You entered: \n%s\n", buf);

		if((p = strchr(buf, '\n')) != NULL) {
			*p = '\0';
		}

		printf("And now it's: \n%s\n", buf);
	}

	for(i = 0; buf[i] != '\0'; i++) {
		buf[i] = toupper(buf[i]);
	}

	printf("As all caps: \n%s\n", buf);

	getCode(buf);

	return 0;
}