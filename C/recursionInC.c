/* 
 * Author: Lochlain Lewis  Date: 2/7/2017
 * 
 * This is a version of the Fibonacci Series 
 * recursive program. The seed numbers are 
 * passed to the function where the where the
 * recursion takes care of the rest. The result 
 * us the seriers up to the 9th index.
 *
 */
#include <stdio.h>

int fibonacci(int i)
{

	if(i == 0)
	{
		return 0;
	}

	if(i == 1)
	{
		return 1;
	}
	return fibonacci(i - 1) + fibonacci(i - 2);
}

int main()
{
	int i;

	for(i = 0; i < 10; i++)
	{
		printf("The test number: %d\n", i);
		printf("%d\t\n", fibonacci(i));
	}

	return 0;
}