#include <stdio.h>
/* This program interacts with the user by introducing
 * itself and asking for the user's name. It then asks 
 * the user to enter a data point. After each data point
 * is entered it asks if the user wishes to continue
 * entering data.
 */

	float data[5];

	float total, average;
	char name[100];
	int a = 0;	// Counter for dividend of average.
	int x = 1;
	int y = 0;	// Stores data pts in array.
	int z = 1;	// Stores answer for adding more data pts.

int main()
{

	//printf("%d, %d, %d, %d, %d, \n", data[0],
			  	   //data[1], data[2], data[3], data[4]);
	printf("Hello! I'm not HAL. Who are you?\n");
	scanf("%s", name);
	//printf("This is the name: %s\n", name);
	printf("Hello %s.\n", name);
	while(x == 1)
	{
		while(y < 5)
		{
			//printf("y first in loop: %d\n", y);
			printf("Please enter a data point:\n");
			scanf("%f", &data[y]);
			a++;
			//printf("a is: %d\n", a);
			//printf("Number entered: %d\n", data[y]);
			//printf("Print data: %d, %d, %d, %d, %d, \n", data[0],
			  	   //data[1], data[2], data[3], data[4]);
			printf("Would you like to enter another?\n"
					"Type 1 for yes, or 0 for no.");
			scanf("%d", &z);
			y++;
			//printf("This is y: %d\n", y);

			if(z == 1)
			{
				continue;
			}
			else break;
			printf("Have a nice day, good bye.\n");

		}
		total = data[0] + data[1] + data[2] + 
					data[3] + data[4];

		average = (data[0] + data[1] + data[2] + 
					data[3] + data[4]) / a;

		printf("Total is: %f\n", total);
		printf("Average is: %f\n", average);
		printf("%s, would you like to run the "
				"program again? Type 1 for yes, "
				"0 for no\n", name);
		scanf("%d", &x);
	}
	printf("Good bye %s.\n", name);
	return 0;
}