#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void strip_newline(char *str, int size) {
	int i;

	//remove null terminator
	for( i = 0; i < size; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			return;              //return simply exits the function
		}
	}
}

int main() {

	int number;
	number = rand()*10;
	printf("%d\n", number);
	int guess;
	int guesses = 0;
	char response[50];
	printf("This program selects a random number between 1 and 10.\n");
	printf("You have to guess what the number is.\n");
	printf("Would you like to play? Enter yes or no.\n");
	fgets(response, 50, stdin);
	strip_newline(response, 50);
	while(strcmp(response, "yes") == 0) {
		if(guesses < 6) {	
			printf("Please enter your guess: \n");
			scanf("%d", &guess);					//put a value in guess
			guesses++;
			if(&number != NULL) {
				if(guess == number) {
					printf("%d is a winner!\n", guess);
				} else if((guess != number) && (guesses < 6)) {
					printf("%d is not it. So sorry. Try again.\n", guess);
				} else {
					printf("You suck at this! Find something else to do.\n");
				}
			}
		}
	}
	
		//getchar();
		return 0;
}