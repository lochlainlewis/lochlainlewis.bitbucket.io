#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int randomnum(int minnum, int maxnum);
char line[80];
int guessnum, guesscount, numtoguess;

int main()
{
	printf("Min: 1, Max: 30; %d\n", randomnum(0, 30));
	//printf("Min: 100, Max: 1000 %d\n", randomnum(100, 1000));
	int numtoguess = randomnum(0, 30);
	guesscount = 1;


	while(1)
	{
		if(guesscount == 5)
		{
			printf("You're not so good at this.\n");
		}
		if(guesscount == 7)
		{
			printf("Geesh! Come on, I don't have\n"
				   "all day!\n");
		}
		if(guesscount == 10)
		{
			printf("You should just give up and \n"
				   "go do something else.");
		}
		printf("The bounds are Min: 1, Max: 30.\n"
			   "So what's your guess?\n"
			   "Your guess count is %d\n", guesscount);
		fgets(line, sizeof(line), stdin);
		sscanf(line, "%d", &guessnum);
		++guesscount;

		if(guessnum == 'q' || guessnum == 'Q')
		{
			break;
		}

		if(guessnum == numtoguess)
		{
			printf("You got it!\n");
			break;
		}
		else if(guessnum < numtoguess)
		{
			printf("Too low!\n");
		}
		else if(guessnum > numtoguess)
		{
			printf("Too high!\n");
		}

	}

	return 0;
}

int randomnum(int minnum, int maxnum)
{
	int result = 0, lownum = 0, hinum = 0;
	if(minnum < maxnum)
	{
		lownum = minnum;
		hinum = maxnum + 1;
	}
	else
	{
		lownum = maxnum + 1;
		hinum = minnum;
	}

	srand(time(NULL));
	result = (rand() % (hinum - lownum)) + lownum;
	return result;

}