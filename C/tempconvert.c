#include <stdio.h>

#define LOWER_LIMIT 0
#define UPPER_LIMIT 5000

int main() {

	double fahr, cel;

	int limit_low = -1;
	int limit_high = -1;
	int step = -1;
	int max_step_size;

	while(limit_low < (int) LOWER_LIMIT) {
		printf("Please give a lower limit, limit >= %d: ", (int) LOWER_LIMIT);
		scanf("%d", &limit_low);
	}

	while((limit_high <= limit_low) || (limit_high > (int) UPPER_LIMIT)) {
		printf("Please give a higher limit, %d < limit <= %d: ", limit_low, (int) UPPER_LIMIT);
		scanf("%d", &limit_high);
	}

	max_step_size = limit_high - limit_low;
	while((step <= 0) || (step > max_step_size)) {
		printf("Please give a step, 0 < step >= %d: ", max_step_size);
		scanf("%d", &step);
	}

	cel = limit_low;

	printf("\nCelsius\t\tFahrenheit");
	printf("\n-------\t\t-------\n");
	while(cel <= limit_high) {
		fahr = (9.0 * cel) / 5.0 + 32.0;
		printf("%f\t%f\n", cel, fahr);
		cel += step;
	}

	printf("\n");
	return 0;

}