/* Author: Lochlain Lewis Date: 02/17/2017
 * Server program for sensor system - sends
 * signal to slave arduino to trigger data
 * polling, retrieves data string into 
 * <inputFile> and saves saves data into
 * <outputFile> on hard drive
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* gets the current time and eliminates spaces
 * and ':' characters */
char *getTime()
{
    char buff[20];
    char *timeStr = (char *)malloc(sizeof(char));
    time_t now = time (0);
    strftime (buff, 20, "%Y-%m-%d %H:%M:%S", localtime (&now));
    //printf ("buff before character elimination: %s\n", buff);
    int j = 0;

    for(int i = 0; i < sizeof(buff); i++)
    {
    	if(buff[i] == ':' || buff[i] == '-' || buff[i] == ' ')
    	{
    		i += 1;
    		timeStr[j] = buff[i];
    	}
    	else
    	{
    		timeStr[j] = buff[i];
    	}
    	j++;
    }

    //printf("timeStr after character elimination: %s\n", timeStr);

    free(timeStr);

    return timeStr;

}

int main()
{
	int printNum = 4;
	FILE *inputFile;
	FILE *outputFile;
	char finalStr[60];
	char newTime[20];
	char *newTimePtr;
	int read;
	int i = 0;
	char fileName[42] = "/Users/lochlainlewis/Documents/testFiles/";
	char fileExt[5] = ".txt";
	char timeStr[100];
	char dateTime[100];

	/* Get the time and build a new file name and path */
	newTimePtr = getTime();
	//printf("newTime is: %s\n", newTimePtr);
	snprintf(timeStr, sizeof timeStr, "%s%s%s", fileName, newTimePtr, fileExt);
	snprintf(dateTime, sizeof timeStr, "%s", newTimePtr);
	printf("File path is: %s\n", timeStr);

	/* Open the port to TX/RX data to/from Arduino */
	inputFile = fopen("/dev/cu.usbmodem1411", "r+");

	/* Check open */
	if(inputFile == NULL)
	{
		printf("Unable to open port\n");
		exit(1);
	}

	/* Send/write something to Arduino */
	/* This is the request for data */
	fputc(printNum, inputFile);

	sleep(5);

	/* Open destination file and check open */
	outputFile = fopen(timeStr, "w");

	if(outputFile == NULL)
	{
		printf("Unable to open output file.\n");
		exit(1);
	}

	/* Grab data from input file */
	/* Put it in a temporary string */
	do
	{
		read = fgetc(inputFile);
		//printf("read is: %d\n", read);
		finalStr[i] = read;
		//printf("finalStr is: %s\n", finalStr);
		i++;
	}while(read != '\0');

	printf("Final string is: \n%s\n", finalStr);

	/* Send temporary file to destination file on hard drive */
	fprintf(outputFile, "Date, Time: %s\n%s\n", dateTime, finalStr);
	fclose(inputFile);
	fclose(outputFile);

	return 0;
}