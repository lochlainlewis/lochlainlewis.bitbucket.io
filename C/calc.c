#include <stdio.h>
#include <string.h>

char line[100];
char operator;
int result, value, base;

int main()
{
	result = 0;

	while(1)
	{
		printf("Result: %d\n", result);
		printf("Enter the operator and a value: \n");

		fgets(line, sizeof(line), stdin);
		sscanf(line, "%c %d", &operator, &value);

		if((operator == 'q' || operator == 'Q'))
		{
			break;
		}

		if(operator == '+')
		{
			result += value;
		}
		else if(operator == '-')
		{
			result -= value;
		}
		else if(operator == '*')
		{
			result *= value;
		}
		else if(operator == '/')
		{
			if(value == 0)
			{
				printf("ERROR: Division by 0\n");
			}
			else
				result /= value;
		}
		else if(operator == '^')
		{
			if(value == 0)
			{
				result = 1;
			}
			else if(value == 1)
			{
				result = result;
			}
			while(value > 1)
			{
				base = result;
				while(value > 1)
				{
				printf("base before equation: %d\n", base);
				printf("result before equation: %d\n", result);
				result = result * base;
				printf("base after equation: %d\n", base);
				printf("result after equation: %d\n", result);
				value -= 1;	
				}

			}
		}
		else
		{
			printf("Unknown operator %c\n", operator);
		}
	}

	return 0;
}