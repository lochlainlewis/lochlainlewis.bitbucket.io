#include <stdio.h>
#include <string.h>

char answer;
char reply;
int cmp;
int mph;
int kph;

int main()
{
	printf("Would you to convert KPH to MPH?\n"
			"Or would you like to convert MPH to KPH?\n"
			"Enter k or m for what you're converting from.\n");
	scanf("%s", &answer);
	//printf("%c\n", answer);
	reply = 'm';

	/* strcmp is looking for an m for MPH. If m is entered it 
	 * returns a 0. If other than 0 is returned the 
	 * program asks the user for KPH input to convert 
	 * KPH to MPH.
	 */
	if(strcmp(&answer, &reply) != 0)
	{
		//if the result of strcmp is not 0, do this.
		printf("Enter the KPH you wish to convert:\n");
		scanf("%d", &kph);
		float tomph = kph * 0.6213712;
		printf("%d KPH equals %f MPH\n", kph, tomph);
		return 0;

	}
	else
		//if the result of strcmp is 0, do this.
		printf("Enter the MPH you wish to convert:\n");
		scanf("%d", &mph);
		float tokph = mph * 1.60934;
		printf("%d MPH equals %f KPh\n", mph, tokph);
		return 0;
}