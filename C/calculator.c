#include <stdio.h>


int main() {

	char o;

	float num1, num2;

	printf("Enter one of the following operatoros, +, -, *, divide : \n");
	scanf("%c", &o);
	printf("Enter two operands: \n");
	scanf("%f%f", &num1, &num2);
	switch(o) {
		case '+':
			printf("%.1f + %.1f = %.1f\n", num1, num2, num1 + num2);
			break;
		case '-' :
			printf("%.1f - %.1f = %.1f\n", num1, num2, num1 - num2);
			break;
		case '*':
			printf("%.1f * %.1f = %.1f\n", num1, num2, num1 * num2);
			break;
		case '/' :
			printf("%.2f / %.2f = %.2f\n", num1, num2, num1/num2);
			break;
		default :
			printf("Error! Operator is incorrect!\n");
			break;
	}
	return 0;
}