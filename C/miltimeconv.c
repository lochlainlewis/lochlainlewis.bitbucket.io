/* Author: Lochlain Lewis Date: 2014.10.04
 * A program to convert military/European style time to 
 * civilian/American style time, or the reverse. 
*/
#include <stdio.h>
#include <string.h>

char am[2];
char answer[2];
char ampmcomp, mil;
int cmpampm, convtime, milcivcomp;


int main()
{
	/* Asks the user if he/she would like to military time or 
	 * civilian time.
	 * Stores result in the variable <answer>. 
	 * If m is entered for military the program returns 0.
	 */
	printf("Would you like to convert military-to-civilian\n"
		"or civilian-to-military time? Enter m for m-to-c\n"
		"or c for c-to-m\n");
	scanf("%s", answer);
	mil = 'm';
	milcivcomp = strcmp(&mil, answer);

	/* Asks the user if the time to convert is a.m. or p.m.
	 * Stores result in <cmpampm>.
	 * If a is entered for a.m. strcmp returns 0.
	 */
	printf("Is the time a.m. or p.m.?\n"
	 		   "Enter a for a.m., or p for p.m.\n");
	scanf("%s", am);
	ampmcomp = 'a';
	cmpampm = strcmp(am, &ampmcomp);

	/* Asks the user to enter the time to convert in a format
	 * that is usable by the program.
	 * Stores the user input in <convtime>.
	 */
	printf("Please enter time to convert. Enter in the \n"
			"following example format: 12:30 enter 1230, \n"
			"for 1:30 enter 130.\n");
	scanf("%d", &convtime);

	/* Takes result of convtime int less than 1200, result of
	 * milcivcomp - m for military returns 0. And cmpampm
	 * a returns 0.
	 * Gives easy instructions how to write or say military
	 * time when given a civilian time that is earlier than
	 * 12-noon.
	 */
	if(milcivcomp != 0 && cmpampm == 0 && convtime < 1200)
	{
		printf("To convert civilian to military time when time \n"
			   "is between 0001 and 1200 noon, add a leading 0 \n."
			   "So 9:30 a.m. becomes 0930.\n");
	}

	/* Takes result of convtime less than 1200, result of
	 * cmpampm is 0, and milcivcmp is m, and informs user 
	 * to drop the leading 0 in military time to arrive at
	 * the proper format for civilian a.m. time.
	 */
	if(milcivcomp == 0 && cmpampm == 0 && convtime < 1200)
	{
		printf("To convert military to civilian time when time \n"
			   "is between 0001 and 1200 noon, drop the leading \n"
			   "0. So 0930 becomes 9:30 a.m.\n");
	}

	/* Takes result of convtime greater than 1200, and where
	 * the result of milcivcomp is m, and subtracts
	 * 1200. Returns the civilian equivalent of military time -
	 * p.m.
	 */
	if(milcivcomp == 0 && cmpampm != 0 && convtime > 1200)
	{
		printf("You entered military time of %d. The civilian \n"
		"equivalent is: %d p.m.\n", convtime, convtime - 1200);
	}

	/* Takes time less than 1200, when the result of cmpampm is not
	 * 0, indicating a p.m. time entry and milcivcomp is not 0, 
	 * indicating civilian to military time conversion. It adds
	 * 1200 to return the correct civilian to military time conv.
	 */
	if(milcivcomp != 0 && cmpampm != 0 && convtime < 1200)
	{
		printf("You entered civilian time of %d p.m. \n"
		"The military equivalent is %d.\n", convtime, convtime + 1200);
	}

	return 0;
}