/* To run a C program with user defined header (.h)
 * files ensure the header and accompanying .c files
 * are formated correctly then run the following from
 * the command-line. Type at the command prompt;
 *
 * gcc <actual file name where main() lives> <filename of .c file(s)> -o main 
 * ./main

 * (for each .c file used add it to the compile-time command 
 * press enter after typing <main> and then, if no issues arise
 * type ./main at the command prompt). Make sure to leave a space between
 * the .c files if there is more than 1.
 */
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "WriteUserInfoToFile.h"
#include "ValidatePassword.h"

int main()
{
	/* the functions called here live in separate .c files, each represented by a 
	 * separate .h file. Only the functions that are visible to the main() function
	 * are included in the function prototypes in the .h file. 
	 */
	char userAnswer[3];
	char yes[] = "yes";
	char ch;
	int i = 0;
	int strCompResult = -1;

	printf("Are you a new user?\n");
	scanf("%s", userAnswer);

	// make the answer all lowercase
	for(int j = 0; userAnswer[j]; j++)
		userAnswer[j] = tolower(userAnswer[j]);

	// compare the answer to the string yes[]
	strCompResult = strcmp(userAnswer, yes);

	// based on the response, go to the appropriate class
	if(strCompResult == 0)
		writeUserNameAndPasswordToFile();
	else
		findUserLogin();

	return 0;
	
}