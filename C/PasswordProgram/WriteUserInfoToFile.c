/* Author: Lochlain Lewis  Date: 10/06/2019
 * 
 * This application writes the user's login and
 * password to a .csv file.
 *
 * Note, this app uses a Mac file path. Replace the
 * filePath string with the path where you want the 
 * dataFile stored. You can also change the file
 * extension, however it is advisable to maintain the
 * .csv extension. CSV files are ubiquitious across
 * OSs. 
 * The user's login is stored in newUser[0], 
 * and the password is stored in newUser[1].
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "GetUserLoginInfo.h"

void writeUserNameAndPasswordToFile()
{	
	FILE *fp;
	char filePath[] = "/Users/lochlainlewis/Documents/cPrograms/SaddlePoint/";
	char fileExt[] = ".csv";
	char fileName[] = "dataFile";
	char finalFilePath[66];
	char insertString[100];
	char comma[] = ",";
	char** newUser = NULL;

	newUser = getUserInfo();
	// build file path	
	snprintf(finalFilePath, sizeof finalFilePath, "%s%s%s", filePath, fileName, fileExt);
	// add user info to insertion string - testBuff is the insertion string
	snprintf(insertString, sizeof insertString, "%s%s%s", newUser[0], comma, newUser[1]);

	/* 
	 * if the file exists open it and insert the string
	 * if the file does not exist - create it and insert the string
	 */
	fp = fopen(finalFilePath, "a+");
	fprintf(fp, "%s\n", insertString);

	fclose(fp);
}