#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "WriteUserInfoToFile.h"
#include "ValidatePassword.h"

/* This class is used to retrieve the user's login
 * information. It used by the WriteUserInfoToFile
 * class and the ValidatePassword class
 *
 * It gets a new user's information and passes it to 
 * the WriteUserInfoToFile class for storage or
 * to the ValidatePassword class for validation.
 * It depends on which class calls the function
 * getUserInfo().
 */
char** getUserInfo()
{
	// allocate some memory for the users login and password
	char **userInfo = (char**)malloc(sizeof(char*)*2);

	for(int lnVar = 0; lnVar < 2; lnVar++)
	{
		userInfo[lnVar] = (char*)malloc(25);
	}

	printf("Enter your user name: \n");
	scanf("%s", userInfo[0]);
	printf("Enter your password: \n");
	scanf("%s", userInfo[1]);	

	return userInfo;
}