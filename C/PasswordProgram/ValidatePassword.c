#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "GetUserLoginInfo.h"

void findUserLogin()
{	
	FILE *fp;
	char** userInfo = NULL;
	char filePath[] = "/Users/lochlainlewis/Documents/cPrograms/SaddlePoint/dataFile.csv";
	char testBuff[25];
	char* token[25];
	char* password;
	char realPass[25];
	bool goodPassword = false;
	int loginCompResult;
	int passwordCompResult;
	int count = 0;
	int i = 0;

	// loop to limit login attempts
	while(count < 3)
	{
		userInfo = getUserInfo();

		fp = fopen(filePath, "r");
	
		if(fp == NULL)
		{
			printf("Error reading file!\n");
			exit(-1);
		}
		// loop to check entire file for login info
		while(!feof(fp))
		{
			char userName[15]= "";
			// get a line from the file
			fscanf(fp, "%s", testBuff);
		 
			// find out where the comma is located
			int index = strcspn(testBuff, ","); 
			// separate the password from the login name
			password = strchr(testBuff, ',');

			// separate the user name from the comma
			for(int i = 0; i < index; i++)
			{
				userName[i] = testBuff[i];
			}
			// login name comparison
			loginCompResult = strcmp(userName, userInfo[0]);

			if(loginCompResult == 0)
			{
				for(int j = 1; j < strlen(password); j++)
				{
					realPass[j-1] = password[j];
				}
				// password comparison
				passwordCompResult = strcmp(realPass, userInfo[1]);

				if(passwordCompResult == 0)
				{
					printf("PASSWORD MATCH, Welcome!\n");
					goodPassword = true;
					break;
				}
				else
				{
					printf("User name and password combination incorrect!!!\n");
				}
			}
		}

		fclose(fp);
		if(goodPassword)
		{
			break;
		}

		if(loginCompResult != 0)
		{
			printf("User name and password combination incorrect!!!\n");
		}

		printf("The count is: %d\n", count);
		count++;
	}
}