import React from "react"
import Layout from "../components/layout"
export default() => (
	<Layout>
		<h1>Hi! I'm a geek. I'm building this fake Gatsby site for the heck of it!</h1>
		<p>
			I'm just expanding my knowledge of new web app frameworks and deployment options.
		</p>
	</Layout>
)
