import React from "react"
import Layout from "../components/layout"
export default() => (
	<Layout>
		<h1>To contact me please use the email address below:</h1>
		<p>
			<a href="mailto:lochlainlewis@gmail.com">lochlainlewis@gmail.com</a>
		</p>
	</Layout>
)