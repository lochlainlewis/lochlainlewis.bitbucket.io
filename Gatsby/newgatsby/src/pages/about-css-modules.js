import React from "react"
import styles from "./about-css-modules.module.css"
import Container from "../components/container"
console.log(styles)
const User = props => (
  <div className={styles.user}>
    <img src={props.avatar} className={styles.avatar} alt="" />
    <div className={styles.description}>
      <h2 className={styles.username}>{props.username}</h2>
      <p className={styles.excerpt}>{props.excerpt}</p>
    </div>
  </div>
)
export default () => (
	<Container>
		<h1>This is a new CSS Module page.</h1>
		<p>There is supposed to be a dorky phrase here.</p>
		<p>Something like, CSS modules are cool. Yah, not!</p>
		 <User
   			username="Jane Doe"
      		avatar="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg"
      		excerpt="I'm Jane Doe. More dork than Bob, consectetur adipisicing elit."
    	/>
    	<User
      		username="Bob Smith"
      		avatar="https://s3.amazonaws.com/uifaces/faces/twitter/vladarbatov/128.jpg"
      		excerpt="I'm Bob Smith, a a dork type of guy. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
    	/>
	</Container>
)