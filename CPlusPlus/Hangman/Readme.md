Hangman Application

Author: Lochlain Lewis  Date:   02/17/2014

This application is a version of the classic spelling game usually
played with a piece of paper and pencil.

The object is for a player to think of a word. Then draw a simple gallows and spaces
for each letter of the word. The opponent's goal is to guess the word. For each 
correct guess the letter is written in its appropiate place. For each incorrect guess
a body part is added to the gallows. The player guessing wins if they guess the word before the body is complete and loses if there is a complete body hanging from the gallows before the word is guessed.

In this rendition of the game the user plays the computer. An external dictionary is
used as the word bank. The computer chooses the word based on a pseudo random number 
(see Choosing A Word).
For each correct guess the computer places the letters where they belong on the spaces. For each wrong guess the computer draws a body part. The user wins if they can guess the word before a complete body is drawn. 

It's important to note that running this program from the command line the user needs
to open the program using the file path to the dictionary file as one of the 
arguments for opening the program. If one is not used the program will throw an
exception.

Choosing A Word
The application uses a word count loop to count the words in the dictionary.
The result of the count is the range for the psuedo random number selection. Once
the number is selected the computer selects the corresponding word and saves it to
memory.