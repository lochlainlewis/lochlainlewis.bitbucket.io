#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <cstdlib>
#include <stdlib.h>
#include <vector>
#include <fstream>

using namespace std;

/* gets random number based on size of dictionary */
int getNumber(int x) {
	int num = rand() % x;
	return num;
}

/* strips all non-alpha characters, converts lowercase to uppercase */
string getUppercase(string str1) {
	int i = 0;
	int len = 0;
	int ascii = 0;
	len = str1.length();
	for(i = 0; i < len; ++i) {
		ascii = int(str1[i]);
		if(ascii > 96 && ascii < 123) {
			ascii -= 32;
			str1[i] = char(ascii);
		}
	}

	i = -1;
	while(i < len) {
		++i;
		ascii = int(str1[i]);
		if((ascii > 90 && ascii < 97) || (ascii < 65 || ascii > 122)) {
			str1.erase(i, 1);
			--i;
			if(str1[i] == '\0' || i == str1.length() - 1) {
				break;
			}
		}
	}
	return str1;
}

/* determines number of unique characters in a string */
int getUnique(string str1) {
	int i = 0;
	int j = 0;
	int count = 0;

	for(i = 0; i < str1.length() - 1; ++i) {
		for(j = i + 1; j < str1.length(); ++j) {
			if(int(str1[i]) == int(str1[j])) {
				str1.erase(j, 1);
				if(str1[i] == '\0' || str1[j] == '\0'){
					break;
				}
			}
		}
	}
	count = str1.length();
	return count;
}

int main(int argc, char* argv[0]) {

	string str;
	string str1;
	string affirm;
	affirm = "yes";
	string neg;
	neg = "no";
	string playerguess;
	string incorrectentry;
	string wrongletter;
	string correctletter;
	string redundletter;
	string word;
	string word1;
	string guess;
	int incorrect = 0;
	int n = 0;
	int i = 0;
	int j = 0;
	int k = 0;
	int count = 0;
	int wordlen = 0;
	int wrongguess = 0;
	int randnum = 0;
	int ascii = 0;
	int guesscheck = 0;
	int guesscount = 0;
	int correctcount = 0;
	int uniqueletters = 0;
	int redundantletter = 0;

	char letter = '_';

	vector<string> dicword;

	srand(time(NULL));

	/* check to make sure correct number of arguments */
	if(argc != 2) {
		cout << "NO DICTIONARY FILE PATH!" << endl;
		exit(EXIT_FAILURE);
	}

	/* open stream to read file, counts the number of words*/
	ifstream fin(argv[1]);

	while(fin >> str) {
		dicword.push_back(str);
		++count;
	}

	/* Gets the random number to pick the random word */
	randnum = getNumber(count);
	//cout << "This is count: " << randnum << endl;

	/* gets a word at the designated index in the file */
	word1 = dicword.at(randnum);
	cout << "From dictionary: " << word1 << endl;

	/* converts to uppercase */
	word1 = getUppercase(word1);

	/* counts unique characters */ 
	uniqueletters = getUnique(word1);
	
	/* user interface */
	cout << "Would you like to play a game?\n" 
	"Enter yes or no." << endl;
	getline(cin, str1);

	int comp = str1.compare(affirm);
	cout << comp << endl;

	if(comp == 0) {
		cout << "How about a nice game of hangman...?" << endl;
		this_thread::sleep_for(std::chrono::seconds(2));
		cout << "Heh..." << endl;
		this_thread::sleep_for(std::chrono::seconds(1));
		cout << "    heh... " << endl;
		this_thread::sleep_for(std::chrono::seconds(1));
		cout << "         heh..." << endl;
		this_thread::sleep_for(std::chrono::seconds(2));
	}
	else {
		cout << "Okay... Have a nice day!" << endl;
		exit(EXIT_SUCCESS);
	}

	cout << endl;
	if(comp == 0) {
		cout << "Okay, two rules; \n1. You lose if you make six wrong guesses.\n" 
					"2. You can only enter letters as guesses." << endl;
		cout << endl;
		cout << endl;
	}

	wordlen = word1.length();
	cout << "I'm thinking of a " << wordlen << " letter word..." << endl;
	cout << endl;

	/* assigns the correct number of under-scores for word length */
	word.assign(wordlen, '_');
	cout << word << endl;

	while(wrongguess < 6) {

		cout << endl;
		cout << endl;

		cout << "So guess a letter... if you dare." << endl;
		getline(cin, guess);

		redundletter = guess;

		/* checks player's input for validity according to rule of play */
		for(i = 0; i < guess.length(); ++i) {
			ascii = int(guess[i]);

			for(j = 0; j < redundletter.length(); ++j) {
				for(k = 0; k < playerguess.length(); ++k) {
					//cout << "guesscount: " << guesscount << endl;
					//cout << "redund: " << redundletter[i] << "playerguess: " << playerguess[k] << endl;
					if(redundletter[i] == playerguess[k]  && guesscount > 0) {
						cout << "You used this guess already!" << endl;
						redundantletter = 1;
						break;
					}
				}	
			}

			playerguess.append(guess);
			++guesscount;
			//cout << "playerguess is: " << playerguess << endl;

			/* converts lowercase to uppercase if meeting single alpah character requirements */
			if(((ascii > 64 && ascii < 91) || ((ascii > 96) && (ascii < 123))) && (guess.length() == 1)) {
				guess = getUppercase(guess);
				//cout << "guess is: " << guess << endl;
			}

			//cout << "guess ascii: " << ascii << endl;
			guesscheck = 0;

			/* notify user of unauthorized entry */
			if((ascii < 64) || (ascii > 90 && ascii < 97) || (ascii > 122) || (guess.length() > 1)) {
				guesscheck = 1;
				cout << "You entered an unauthorized guess!" << endl;
				cout << "Please enter one letter only." << endl;
				break;
			}
		}

		/* updates unauthorized entries */
		if(guesscheck == 1) {
			incorrectentry.append(guess + ' ');
			//cout << "incorrect entry; " << incorrectentry << endl;
		}

		/* finds first occurrence of character */
		n = word1.find_first_of(guess);
		//cout << "this is n: " << n << endl;

		/* finds all occurrences of character */
		if(n > -1 && guesscheck == 0) {
			//cout << "first " << guess << " found at position " << n + 1 << endl;

			for(i = 0; i < wordlen; ++i) {
				string temp;
				temp = word1[i];
				int result = guess.compare(temp);

				if(result == 0) {
					word.replace((i), 1, guess);
				}
			}

			if(redundantletter == 0) {
				correctletter.append(guess + ' ');
				++correctcount;			
			}
			redundantletter = 0;

			cout << endl;
			cout << endl;			
			//cout << word << endl;
			cout << endl;
			cout << endl;			
		}

		/* if guess is incorrect - inform player, increase wrong guess count */
		else if (n == -1 && guesscheck == 0 && redundantletter == 0) {
			
			wrongletter.append(guess + ' ');
			++ incorrect;

			cout << endl;
			cout << endl;
			cout << "So sorry! Your guess is... " << endl;
			this_thread::sleep_for(std::chrono::seconds(2));
			cout << endl;
			cout << endl;
			cout << "INCORRECT!!!" << endl;
			cout << endl;
			cout << endl;
			if(incorrect == 1) {
				cout << "     * * * * * *" << endl;
				cout << "     *         *" << endl;
				cout << "     *         *" << endl;
				cout << "    * *        *" << endl;
				cout << "  *     *      *" << endl;
				cout << "  *     *" << endl;
				cout << "    * *" << endl;
			}
			if(incorrect == 2) {
				cout << "       * * * * * *" << endl;
				cout << "       *         *" << endl;
				cout << "       *         *" << endl;
				cout << "      * *" << endl;
				cout << "    *     *" << endl;
				cout << "    *     *" << endl;
				cout << "      * *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;				
			}
			if(incorrect == 3) {
				cout << "       * * * * * *" << endl;
				cout << "       *         *" << endl;
				cout << "       *         *" << endl;
				cout << "      * *" << endl;
				cout << "    *     *" << endl;
				cout << "    *     *" << endl;
				cout << "      * *" << endl;
				cout << "   *****" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;	
			}
			if(incorrect == 4) {
				cout << "       * * * * * *" << endl;
				cout << "       *         *" << endl;
				cout << "       *         *" << endl;
				cout << "      * *" << endl;
				cout << "    *     *" << endl;
				cout << "    *     *" << endl;
				cout << "      * *" << endl;
				cout << "   *********" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;	
			}
			if(incorrect == 5) {
				cout << "       * * * * * *" << endl;
				cout << "       *         *" << endl;
				cout << "       *         *" << endl;
				cout << "      * *" << endl;
				cout << "    *     *" << endl;
				cout << "    *     *" << endl;
				cout << "      * *" << endl;
				cout << "   *********" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "      *" << endl;
				cout << "     *" << endl;
				cout << "    *" << endl;
			}
			if(incorrect == 6) {
				cout << "       * * * * * *" << endl;
				cout << "       *         *" << endl;
				cout << "       *         *" << endl;
				cout << "      * *" << endl;
				cout << "    * X X *" << endl;
				cout << "    *     *" << endl;
				cout << "      * *" << endl;
				cout << "   *********" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "       *" << endl;
				cout << "      * *" << endl;
				cout << "     *   *" << endl;
				cout << "    *     *" << endl;	
				cout << endl;
				cout << endl;
				cout<< "SO SORRY... YOU LOSE!!!" << endl;
				cout << "The word was " << word1 << " Better luck next time!" << endl;
			}

			++wrongguess;
			
		}
		redundantletter = 0;
		cout << word << endl;
		cout << "Correct letters: " << correctletter << endl;
		cout << "Wrong letters: " << wrongletter << endl;
		cout << "Unauthorized entries: " << incorrectentry << endl;
		cout << "Number of wrong guesses: " << wrongguess << "\n"<< endl;

		if(uniqueletters == correctcount) {
			cout << "YOU WIN!!!" << endl;
			exit(EXIT_SUCCESS);
		}
	}
	fin.close();
	return 0;
}
